package org.usfirst.frc.team702.robot;

import java.util.Arrays;
import java.util.TimerTask;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.Timer;

/**
 * @author sbaron
 *
 */
public class LidarLite_V1 {

	
	private static final int DEFAULT_I2C_ADDRESS = 0x62;
	
	public boolean flg_debug = true;
	private int timesInBlock;
    
    /**********************************************************
	 *  Threading
	 **********************************************************/
    java.util.Timer m_scheduler;
    private double m_period;
    
    /**********************************************************
	 *  I2C
	 **********************************************************/
    // The I2C bus object
    private I2C m_i2c_bus;
    private int m_i2c_address;
    
    /**
     * Mode Control Register - Commands the LIDAR Lite to change operating modes
     *  using a bit-packed control word;
     *  
     * Bit 0 - Preamp Off - Shutdown Preamp between measurements
     * Bit 1 - Clk Shut - External Clock Shutdown (Not Used)
     * Bit 2 - FPGA Sleep - Full FPGA sleep after measurement
     * Bit 3 - DET OFF - Turns off detector bias after measurement
     * Bit 4 - N/A
     * Bit 5 - Velocity Scale Factor
     * Bit 6 - Inhibit Reference
     * Bit 7 - Velocity
     * 
     **/
    private static int CR_MODE_CONTROL_04 = 0x04;
    
    /**
     * Mode Control Register - Commands the LIDAR Lite to change operating modes
     *  using a bit-packed control word;
     *  
     * Bit 0 - OSC Disable - Disable oscillator reference (Not Used)
     * Bit 1 - RCVR PWR Disable - Turns on receiver regulator
     * Bit 2 - SLEEP - Processor Sleep, reduces power to 20mA
     * Bit 3 - Det Bias Disable - 
     * Bit 4 - N/A
     * Bit 5 - N/A
     * Bit 6 - N/A
     * Bit 7 - N/A
     * 
     */
    private static int CR_POWER_CONTROL_101 = 0x65;
    
    /**
     * External interface for the scheduled polling data
     */
    private double m_distance = 0;
    private boolean m_data_valid = false;
    private double m_data_timestamp = 0;
    
    

	/**
	 * @author sbaron
	 * PixyTask is the private scheduler within PixyCMU5 that 
	 * automatically performs I2C reads to get the frame data
	 * from the connected Pixy device.
	 */
	private class LidarLiteV1_Task extends TimerTask {

        private LidarLite_V1 m_lidarlitev1;

        public LidarLiteV1_Task(LidarLite_V1 lidar) {
          if (lidar == null) {
            throw new NullPointerException("Given LidarLite Instance was null");
          }
          this.m_lidarlitev1 = lidar;
        }

        @Override
        public void run() {
        	m_lidarlitev1.getData();
        }
      }
    
    
    /**
     * The constructors for the class. Initialize any data and the bus
     * @param i2c_address_in
     */
    public LidarLite_V1(int i2c_address_in)
    {
    	timesInBlock = 0;
    	m_i2c_address = i2c_address_in;
    	m_i2c_bus = new I2C(I2C.Port.kOnboard, m_i2c_address);
    }
    
    /**
     * The constructors for the class. Initialize any data and the bus
     * @param i2c_address_in
     */
    public LidarLite_V1(int i2c_address_in, double period)
    {
    	timesInBlock = 0;
    	m_i2c_address = i2c_address_in;
    	m_i2c_bus = new I2C(I2C.Port.kOnboard, m_i2c_address);
    	m_period = period;
    	
    	m_scheduler = new java.util.Timer();
    	// Schedule the LidarLite task to execute every <period> seconds
    	m_scheduler.schedule(new LidarLiteV1_Task(this), 5000L, (long) (m_period * 1000));
    }
    
    
    /**
     * Start a thread to read data from the Lidar periodically
     * 
     * @param period - period in seconds to schedule update
     */
    public void start(double period)
    {
    	// Schedule the LidarLite task to execute every <period> seconds
    	if(m_scheduler == null)
    	{
    		m_period = period;
    		System.out.println("Attempting to enable Pixy at a " + Double.toString(period) + " second rate.");
    		m_scheduler = new java.util.Timer();
    		m_scheduler.schedule(new LidarLiteV1_Task(this), 0L, (long) (m_period * 1000));
    		
    	} else {
    		System.out.println("LidarLiteV1 Thread already scheduled. Stop before starting a new thread.");
    	}
    }
    
    
    /**
     * Cancel a running timer and attempt to null the variable
     */
    public void stop()
    {
    	// If the timer object is not null, cancel the scheduler
    	if(m_scheduler != null)
    	{
    		System.out.println("Attempting to disable LidarLiteV1 auto polling.");
    		m_scheduler.cancel();
    		m_scheduler.purge();
    		// TODO: see if this really frees the memory and assigns it to NULL
    		m_scheduler = null; // this may break things or cause memory leaks, verify
    	} else {
    		// nothing to do
    	}
    }
    
    
    
    /**
     * Polls the I2C bus to see if a device with the address specified is present
     * @return - true if present, false if not detected
     */
    public boolean isDevicePresent()
    {
    	return !m_i2c_bus.addressOnly();    	
    }
    
    
    /**
     * Attempts to read data from the LidarLiteV1. If the I2C read fails or if 
     * all data that is returned is 0, a Double.NaN will be returned. If the read
     * is a success, the distance detected will be returned and the corresponding 
     * values in the class will be set.
     * 
     * @return - distance detected in cm
     */
    public double getData()
    {
    	byte readBuffer[] = new byte [8];
    	byte zeroArray[] = {0,0,0,0,0,0,0,0};
    	byte readReg[] = {(byte)0x8f};
    	
    	// Queue the Lidar to update the distance reading
    	m_i2c_bus.write(0x00, 0x04);
    	
    	// Wait for the distance read to complete and be loaded into the Lidar controller
    	Timer.delay(0.02);
    	// Read data from the i2c bus
    	m_i2c_bus.transaction(readReg,1,readBuffer,8);
    	    	
    	// If every element in the readBuffer is zero, no data was returned on the i2c bus
    	if(Arrays.equals(zeroArray, readBuffer))
    	{
    		System.out.println("Lidar Lite V1 - Read Failed! All elements returned were 0!");
        	setDataValid(true);
    	} else {
    		setDataTimestamp(Timer.getFPGATimestamp());
        	setDataValid(true);
    	}
    	
    	// The contents of byte 5 and 6 represents the distance in CM
    	int processedDistance = ((readBuffer[5] << 8) | readBuffer[6]) & 0xFFFF;
    	
    	// Set the class variables
    	
    	setDistance((double)processedDistance);
    	
    	if(flg_debug)
    	{
        	String readBuffer_char = "";
        	
    		// Interate through the received data and convert from unsigned byte to string
    		for(int idx = 0; idx < readBuffer.length; idx++)
    		{
    			readBuffer_char += Integer.toString(readBuffer[idx] & 0xFF) + " ";
    		}
    		// Print the raw buffer to the console
    		System.out.println(readBuffer_char);
    		
    		System.out.println( "Raw Data: [0,1] "
					+ Integer.toString(readBuffer[5] & 0xFF) 
					+ Integer.toString(readBuffer[6] & 0xFF) +
					" Data Received: " + Integer.toString(processedDistance));
    	}
    	
    	System.out.println("Lidar Lite V1 - Distance: " + Double.toString(processedDistance));

    	
    	return (double)processedDistance;
    }
    
    
    /************************************************************
     * Getters and Setters
     ************************************************************/
    
    public synchronized double getDistance() {
		return m_distance;
	}

	private synchronized void setDistance(double processedDistance) {
		this.m_distance = processedDistance;
	}

	public synchronized boolean isDataValid() {
		return m_data_valid;
	}

	private synchronized void setDataValid(boolean m_data_valid) {
		this.m_data_valid = m_data_valid;
	}

	public synchronized double getDataTimestamp() {
		return m_data_timestamp;
	}
	
	/**
	 * Returns the number of seconds since the last successful data update
	 * @return
	 */
	public double getDataAge() {
		return Timer.getFPGATimestamp() - getDataTimestamp();
	}

	private synchronized void setDataTimestamp(double m_data_timestamp) {
		this.m_data_timestamp = m_data_timestamp;
	}

}
