package org.usfirst.frc.team702.robot;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

// Extend the RobotDrive to override @c mecanumDrive_Cartesian to add rate limits to the inputs.
/**
 * @author sbaron
 *
 */
public class RAWDrive extends RobotDrive  implements PIDOutput {

	public enum moveOrTurn{
		MOVE,
		TURN
	}

	public static class tankMovement
	{
		public double y_move;
		public double x_move;
		public double x_turn;
	}
	public static class tankStickPosition
	{
		public double rightStick;
		public double leftStick;
	}

	private RateLimiter limitedAngleOutput;

	double m_oldLeft = 0;
	double m_oldRight = 0;
	double m_oldRotation = 0;

	double m_oldTime;
	double m_durationLimit = .1;
	double m_strafeRampRate = .3;
	double m_forwardRampRate = 1;

	double m_reverseRampRate = .3;
	double m_rotationRampRate = .3;
	
	double m_gamma = 2.0;
	
	// Setpoints to trigger autonomous movement
	private boolean m_enablePID = false;

	public double angleSetpoint;
	public double speedSetpoint;
	// Rotation control.
	AHRS navX;

	boolean needsTarget;
	float m_targetRotation = 0;

	// PID Controllers
	AnglePIDController anglePIDLoop;

	// PID Constants
	double kP = .00195, kI = 0.00, kD = 0.00, kF = 0.0;
	double kToleranceDegrees = 2.0;

	private double turnRate;

	public RAWDrive(SpeedController frontLeftMotor,
			SpeedController rearLeftMotor, SpeedController frontRightMotor,
			SpeedController rearRightMotor) {
		super(frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor);

		m_oldLeft = 0;
		m_oldRight = 0;	
		m_oldRotation = 0;
		m_oldTime = Timer.getFPGATimestamp();
		
		initSmartDashboard();
		
		// Instantiate and initialize the rate limiter
		//limitedAngleOutput = new RateLimiter(0.5, 0.5, 1.0, 1.0);
		//limitedAngleOutput.initLinearLimiter();
	}

	public void initSmartDashboard()
	{
		// RAWDrive PID settings
	    SmartDashboard.putBoolean("raw/AutoRotateEnabled", false);
		SmartDashboard.putNumber("raw/LeftStickCommand", 0);
		SmartDashboard.putNumber("raw/RightStickCommand", 0);
		SmartDashboard.putNumber("raw/P", 0);
		SmartDashboard.putNumber("raw/I", 0);
		SmartDashboard.putNumber("raw/D", 0);
		SmartDashboard.putNumber("raw/F", 0);
		SmartDashboard.putNumber("raw/Error", 0);
		SmartDashboard.putNumber("raw/Setpoint", 0);
		SmartDashboard.putNumber("raw/Avg. Error", 0);
		SmartDashboard.putNumber("raw/Angle Output", 0);
		SmartDashboard.putNumber("raw/Angle Reading", 0);
	}
	
	public void updateSmartDashboardData()
	{
		SmartDashboard.putBoolean("raw/AutoRotateEnabled", anglePIDLoop.isEnabled());
		SmartDashboard.putNumber("raw/P", anglePIDLoop.getP());
		SmartDashboard.putNumber("raw/I", anglePIDLoop.getI());
		SmartDashboard.putNumber("raw/D", anglePIDLoop.getD());
		SmartDashboard.putNumber("raw/F", anglePIDLoop.getF());
		SmartDashboard.putNumber("raw/Error", anglePIDLoop.getError());
		SmartDashboard.putNumber("raw/Setpoint", anglePIDLoop.getSetpoint());
		SmartDashboard.putNumber("raw/Avg. Error", anglePIDLoop.getAvgError());
		SmartDashboard.putNumber("raw/Angle Output", turnRate);
		SmartDashboard.putNumber("raw/Angle Reading", navX.getYaw());
	}
	
	public void getSmartDashboardData()
	{
		double tmpP = SmartDashboard.getNumber("raw/P", -1);
		if(tmpP != -1)
		{
			setkP(tmpP);
		}
		
		double tmpI = SmartDashboard.getNumber("raw/I", -1);
		if(tmpI != -1)
		{
			setkI(tmpI);
		}
		
		double tmpD = SmartDashboard.getNumber("raw/D", -1);
		if(tmpD != -1)
		{
			setkD(tmpD);
		}

		/*
		 * TODO: implement this.setkF()
		double tmpF = SmartDashboard.getNumber("raw_F", -1);
		if(tmpF != -1)
		{
			setkF(tmpF);
		}*/

	}
	
	public static tankMovement calculateMovementFromSticks(double leftValue, double rightValue)
	{
		/* *
		 * The orientation of the robot controls in the method are as follows:
		 *   +Y axis is Robot Forward
		 *  -Y axis is Robot Reverse
		 *  +X axis is Robot Right
		 *  -X axis is Robot Left
		 *  
		 *  -----------
		 *  |         |
		 *  |   +Y    |
		 *  |    |    |
		 *  |-X.....+X|
		 *  |    |    |
		 *  |   -Y    |
		 *  |         |
		 *  -----------
		 *  
		 * */

		tankMovement result = new tankMovement();

		double y_move = 0;
		double x_move = 0;
		double x_turn = 0;

		/* *
		 * To find the net amount of the Y vector (amount both joysticks are 
		 * positive or negative) we need to determine if the signs are the same;
		 */

		if(Math.signum(leftValue)==Math.signum(rightValue))
		{
			if(Math.signum(leftValue) == 1.0)
			{ // Net forward

				// Assign the value of right and left to Y (common forward direction)
				if(leftValue > rightValue)
				{ //If the left stick is MORE positive, use the right stick
					y_move = rightValue;
				}
				else
				{ //If the right stick is MORE positive, use the left stick
					y_move = leftValue;
				}

				/* *
				 * Assign the difference of left and right to X (rotation bias)
				 * E.g., If right stick is greater than left stick, the robot should
				 *       turn to the left which is indicated by a negative X value.
				 *       Therefore leftValue - rightValue (smaller - larger) will 
				 *       yield a net negative value.
				 *       Contrary to that, if the left stick is greater than the right
				 *       stick, the robot should turn to the right which is indicated 
				 *       by a positive X value.
				 */
				x_move = leftValue - rightValue;
				x_turn = x_move;
			}
			else if(Math.signum(leftValue) == -1.0)
			{ // Net backwards
				// Assign the smallest difference between right and left
				if(leftValue < rightValue)
				{ // If the left stick is MORE negative, use the right stick
					y_move = rightValue;
				}
				else
				{ // If the right stick is MORE negative, use the left stick
					y_move = leftValue;
				}

				/* *
				 * Assign the difference of right and left to X (rotation bias)
				 * E.g., If right stick is more negative than left stick, the 
				 *       robot should move back and to the left, but turn towards
				 *       the right similar to parallel parking a car. This is 
				 *       indicated by a negative value in x_move and a positive
				 *       value in x_turn.
				 *       
				 *       This can be accomplished by setting 
				 *       x_move to rightValue - leftValue. If rightValue = -1, 
				 *       and leftValue = -0.25 then -1 -(-0.25) = -0.75. This can 
				 *       be seen as most of the motion is going towards turning
				 *       and a smaller amount towards moving backwards.
				 *       
				 *       To set x_turn we do the opposite. leftValue - rightValue
				 *       If rightValue = -0.25 and leftValue = -1, 
				 *       then -0.25-(-1) = +0.75 indicating a turn in the +X direction.
				 */
				x_move = rightValue - leftValue;
				x_turn = leftValue - rightValue;

			}
			else
			{ // No movement

			}


			// END (Math.signum(leftValue)==Math.signum(rightValue))
		} else { 
			/* *
			 * Sticks do not have a common direction
			 *  - It MAY be safe to assume we don't need to calculate 
			 *    any net forward or reverse motion but this may not
			 *    be true depending on how the drivers want it to operate.
			 *  - For a first attempt, assume that the net difference
			 *    will be equally split and applied to both wheels for 
			 *    turning only. 
			 *    E.g., Left stick is +0.6, Right Stick is -0.2. Difference
			 *    is 0.8, so apply that to the motors as +0.4, -0.4 for an
			 *    even turn.
			 */

		}
		y_move = leftValue-rightValue;


		// Pack results and return them
		result.x_move = x_move;
		result.y_move = y_move;
		result.x_turn = x_turn;

		return(result);
	}

	/**
	 * Overloaded wrapper for f_calculateSticksFromMovement which assumes that the dX parameter
	 * indicates turning and not movement
	 * 
	 * @param dY - Percent change forward or backward
	 * @param dX - Percent change turning left or right
	 * @return Joystick Positions for a Tank Drive robot
	 */
	public static tankStickPosition calculateSticksFromMovement(double dY, double dX)
	{
		return RAWDrive.f_calculateSticksFromMovement( dY,  dX, RAWDrive.moveOrTurn.TURN);
	}

	/**
	 * Overloaded wrapper for f_calculateSticksFromMovement which allows the dX
	 * parameter to indicate Movement or Turning of the robot based on the moveOrTurn
	 * argument
	 * 
	 * @param dY - Percent change forward or backward
	 * @param dX - Percent change turning left or right
	 * @param moveOrTurn - True for turning, false for movement
	 * @return Joystick Positions for a Tank Drive robot
	 */
	public static tankStickPosition calculateSticksFromMovement(double dY, double dX, moveOrTurn MOT)
	{
		return RAWDrive.f_calculateSticksFromMovement( dY,  dX, MOT);
	}

	private static tankStickPosition f_calculateSticksFromMovement(double dY, double dX, moveOrTurn MOT)
	{
		tankStickPosition result = new tankStickPosition();

		double leftStick = 0;
		double rightStick = 0;

		double limitCurve = 5;
		double xMinPercent = 0.3;
		double yBias = 0;
		double ySlope = 1;
		double yMax = 1;
		double yMin = -1;

		double xBias = 0;
		double xSlope = 1;
		double xMax = 1;
		double xMin = -1;
		
		// Bound dX and dY
		dX = ((dX < xMin) ? xMin : dX) > xMax? xMax : dX;
		dY = ((dY < yMin) ? yMin : dY) > yMax? yMax : dY;

		// Calculate the magnitude of the movement vector to bound to the unit circle
		double vecMag = Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));

		// If the magnitude of the movement is greater than 1
		// scale the dX and dY vectors based on the vector magnitude
		if(vecMag > 1)
		{
			dX /= vecMag;
			dY /= vecMag;
		}
		/* *
		 * Apply the forward or backward motion to the virtual stick positions
		 * and linearly scale them with a slope and bias. Movement should be 
		 * capped at +-0.5 so turning can be biased on top of it with +-0.5
		 * 
		 *      Y    1   2
		 *      |  /   ,
		 *      | /  ,
		 *      |/,
		 * .............X
		 *   , /|
		 * ,  / |
		 *   /  |
		 *   
		 * */
		rightStick = ySlope*dY+yBias;
		leftStick = ySlope*dY+yBias;

		if(dX == 0)
		{
			result.leftStick = leftStick;
			result.rightStick = rightStick;
			
			return(result);
		}


		if(dX!=0 && Math.abs(dX) < xMinPercent)
		{
			dX = xMinPercent*2*Math.signum(dX)*(1-1/(limitCurve*Math.abs(dX)+1));
		}

		rightStick = rightStick - (xSlope*dX+xBias)/2;
		leftStick = leftStick + (xSlope*dX+xBias)/2;


		// Scale the left and right stick positions so they can cover their maximum range
		double scaleRight = 2;
		double scaleLeft = 2;

		// If the right stick is out of bounds
		if(rightStick > 1 || rightStick < -1)
		{
			// Calculate the scale factor to correct it
			scaleRight = 1/Math.abs(rightStick);
		}

		rightStick *= scaleRight;
		leftStick *= scaleRight;

		if(leftStick > 1 || leftStick < -1)
		{
			// Calculate the scale factor to correct it
			scaleLeft = 1/Math.abs(leftStick);
		}

		rightStick *= scaleLeft;
		leftStick *= scaleLeft;

		// Pack results and return them

		// If the net motion is backwards, and the moveOrTurn argument is True which indicates movement
		// then switch the left and right stick values
		/*
		if(dY < 0 && MOT == RAWDrive.moveOrTurn.MOVE)
		{
			result.leftStick = rightStick;
			result.rightStick = leftStick;
		} else {
			result.leftStick = leftStick;
			result.rightStick = rightStick;
		}
		 */
		
		result.leftStick = leftStick;
		result.rightStick = rightStick;
		
		return(result);
	}


	/**
	 * Returns the angle setpoint of the RAWDrive
	 * 
	 * @return angleSetpoint +- 180 degrees
	 */
	public double getAngleSetpoint() {
		return angleSetpoint;
	}


	public double getSpeedSetpoint() {
		return speedSetpoint;
	}

	
	/**
	 * Indicates if the PID loop is enabled and running.
	 * 
	 * @return boolean
	 */
	public boolean isAutoEnable() {
		if(this.navX == null)
		{
			System.out.println("RESET NAVX!!!!!!!!!!!!!!!!!!!!!!");
			return false;
		}
			
		if(anglePIDLoop.isEnabled() && m_enablePID)
		{
			return true;
		} else {
			// If there is a conflict in the settings, fix them
			this.setAutoEnable(false);
			return false;
		}
	}

	
	/**
	 * Indicates if the PID loop error is within tolerance
	 * i.e., The robot has turned to the correct angle
	 * 
	 * @return boolean
	 */
	public boolean isComplete()
	{
		//if(anglePIDLoop.getError() < 10)
		//	return true;
		
		return anglePIDLoop.onTarget();
	}

	/* 
	// Limit the rate at which the new value can change relative to the old value.
	double linearLimit(double newValue, double oldValue, double reverseRate, double forwardRate, double duration) {
		boolean goingForward = newValue < 0;
		double rate = goingForward ? forwardRate : reverseRate;
		// This doesn't handle direction change, but allows immediate braking. Needs to be fixed.
		if (Math.abs(newValue) < Math.abs(oldValue)) return newValue;
		// To prevent big jumps.
		duration = Math.min(duration, m_durationLimit); 
		double limit = Math.abs(duration / rate);
		// Maybe we should assert for this; we shouldn't be driving the robot 2x in the same loop.
		if (limit == 0) return oldValue;
		double delta = (newValue - oldValue);
		if (Math.abs(delta) > limit) {
			delta = limit * Math.signum(delta);
		}
		return oldValue + delta;
	}

	 @Override
  public void mecanumDrive_Cartesian(double x, double y, double rotation, double gyroAngle) {
    if (navX != null && rotation == 0) {
      if (needsTarget) {
        m_targetRotation = navX.getYaw();
        needsTarget = false;
      } else {
        rotation = (m_targetRotation - navX.getYaw()) / 180.;
      }
    } else {
      needsTarget = true;
    }
    double newTime = Timer.getFPGATimestamp();
    double duration = newTime - m_oldTime;
    double newX = linearLimit(x, m_oldX, m_strafeRampRate, m_strafeRampRate, duration);
    double newY = linearLimit(y, m_oldY, m_reverseRampRate, m_forwardRampRate, duration);
    double newRotation = linearLimit(rotation, m_oldRotation, m_rotationRampRate, m_rotationRampRate, duration);
    super.mecanumDrive_Cartesian(newX, newY, newRotation, gyroAngle);
    m_oldTime = newTime;
    m_oldX = newX;
    m_oldY = newY;
    m_oldRotation = newRotation;
  }*/


	// Returns an angle between -180 and 180 degrees
	public static float normalizeAngle(float angle)
	{
		// reduce the angle  
		angle =  angle % 360; 

		// force it to be the positive remainder, so that 0 <= angle < 360  
		angle = (angle + 360) % 360;  

		// force into the minimum absolute value residue class, so that -180 < angle <= 180  
		if (angle > 180)  
			angle -= 360;  

		return angle;
	}

	@Override
	public void pidWrite(double output) {
		
		// TODO: Verify this works as expected
		if(this.m_frontLeftMotor.getInverted())
		{
			turnRate = -output;
		} else {
			turnRate = output;
		}
		
		
		// turnRate = limitedAngleOutput.limit(output, Timer.getFPGATimestamp());
		
		
		//turnRate = output;
		Timer.delay(0.005);
	}

	/**
	 * Set an angle setpoint for the auto-rotate target. The angle 
	 * is error checked and normalized to insure it is within bounds.
	 * 
	 * @param angleSetpoint
	 */
	public void setAngleSetpoint(double angleSetpoint) {
		this.angleSetpoint = this.normalizeAngle((float)angleSetpoint);
	}

	
	/**
	 * Enables or disables the automatic rotation function and sets the PID target
	 * to the angleSetpoint property if enabling
	 * 
	 * @param autoEnable - boolean
	 */
	public void setAutoEnable(boolean autoEnable)
	{
		
		if(anglePIDLoop == null && navX == null)
		{
			System.out.println("INITIALIZE NAVX!!!");
			return;
		}
		// If autoEnable is true, enable the PID loop and 
		// set the setpoint to what is assigned in the class
		if(autoEnable)
		{ // Enable the turn controller
			this.navX.resetDisplacement();
			
			this.anglePIDLoop.reset();
			this.anglePIDLoop.setSetpoint(this.angleSetpoint);
			this.anglePIDLoop.enable();

		} else { // Disable the turn controller
			this.anglePIDLoop.reset();
			this.anglePIDLoop.disable();
		}

		this.m_enablePID = autoEnable;
	}

	
	/**
	 * Assigns the IMU object as the PID source. In this case we expect a navX
	 * Error checking is done to insure that the IMU object has been initalized
	 * 
	 * @param imu - AHRS
	 * @return - true for success, false for failure
	 */
	public boolean setImu(AHRS imu) {
		if(navX == null && imu != null)
		{
			navX = imu;
			navX.setPIDSourceType(PIDSourceType.kDisplacement);

			anglePIDLoop = new AnglePIDController(kP, kI, kD, navX, this);
			anglePIDLoop.setInputRange(-180.0, 180.0);
			anglePIDLoop.setOutputRange(-1.0, 1.0);
			anglePIDLoop.setAbsoluteTolerance(kToleranceDegrees);
			anglePIDLoop.setToleranceBuffer(1);
			anglePIDLoop.setContinuous(true);

			//LiveWindow.addActuator("DriveSystem", "RotateController", anglePIDLoop);

			return true;
		} else {
			return false;
		}
	}

	@Override
	/**
	 * Provide tank steering using the stored robot configuration. This function
	 * lets you directly provide joystick values from any source.
	 *$
	 * @param leftValue The value of the left stick.
	 * @param rightValue The value of the right stick.
	 */
	public void tankDrive(double leftValue, double rightValue) {
		double leftStickValue = leftValue;
		double rightStickValue = rightValue;

		// If the navX or the PID loop objects are not initialized correctly then
		// just directly drive the motors with the joystick inputs
		if(navX == null || anglePIDLoop == null)
		{
			super.tankDrive(leftStickValue, rightStickValue);
			return;
		}

		// If the auto rotate loop is enabled
		if(anglePIDLoop.isEnabled()) 
		{
			// If between the last call and the current call, the PID loop was disabled
			if(!m_enablePID)
		    {
				System.out.println("Auto Rotate Disabled! Disabling Auto Rotate! ");
				this.setAutoEnable(false);

				leftStickValue = leftValue;
				rightStickValue = rightValue;

			} else if(!(Math.abs(leftValue) < 0.2 && Math.abs(rightValue) < 0.2)){
				// If either stick has been moved, then disable auto rotate
				System.out.println("Sticks Moved! Disabling Auto Rotate! ");
				this.setAutoEnable(false);

				leftStickValue = leftValue;
				rightStickValue = rightValue;
			} else {
				tankStickPosition stickPos = calculateSticksFromMovement(0, turnRate);

				if(this.isComplete()) // || anglePIDLoop.getError() < 1)
				{
					this.setAutoEnable(false);
					leftStickValue = 0;
					rightStickValue = 0;
				} else {
					leftStickValue = stickPos.leftStick;
					rightStickValue = stickPos.rightStick;
				}
			}


		} else { // If auto rotate is disabled
			leftStickValue = leftValue;
			rightStickValue = rightValue;
		}

		SmartDashboard.putNumber("LeftStickCommand", leftStickValue);
		SmartDashboard.putNumber("RightStickCommand", rightStickValue);
		
		m_oldRight = rightStickValue;
		m_oldLeft = leftStickValue;
		super.tankDrive(leftStickValue, rightStickValue);
	}
	
	
	/**
	 * Drive straight forward or backward and use the navx to compensate for drift
	 * @param speed
	 */
	public void driveStraight(double speed)
	{
		if(navX != null)
		{
			this.drive(speed, (navX.getYaw()/(m_gamma*180)));
		}
	}

	
	/**
	 * Sets the PID setpoint and enables the PID loop to turn the robot to 
	 * the specified angle
	 * 
	 * @param targetAngle - the target angle, automatically normalized
	 * @param relative - if true, set the angle relative to the robots current heading
	 */
	public void turnToAngle(float targetAngle, boolean relative)
	{
		
		if(navX == null)
		{
			System.out.println("NAVX NEEDS TO BE INITIALIZED!!");
			return;
		}
		
		// If the relative flag is set, add the targetAngle to the current angle
		if(relative)
		{

			targetAngle = normalizeAngle(navX.getYaw()+normalizeAngle(targetAngle));
		}
		
		System.out.println("Turning to angle: " + Float.toString(targetAngle));

		// Assign the target angle (now absolute) to the angleSetpoint
		this.setAngleSetpoint(targetAngle);
		
		System.out.println("Angle setpoint: " + Double.toString(this.angleSetpoint));
		this.setAutoEnable(true);
	}
	
	public void setkP(double kP)
	{
		this.kP = kP;
		if(this.anglePIDLoop != null)
		{
			this.anglePIDLoop.setPID(this.kP, this.kI, this.kD);
		}
	}
	
	public double getkP()
	{
		if(this.anglePIDLoop != null)
		{
		return this.anglePIDLoop.getP();
		} else {
			return this.kP;
		}
	}
	
	public void setkI(double kI)
	{
		this.kI = kI;
		if(this.anglePIDLoop != null)
		{
		this.anglePIDLoop.setPID(this.kP, this.kI, this.kD);
		}
	}
	
	public double getkI()
	{
		if(this.anglePIDLoop != null)
		{
		return this.anglePIDLoop.getI();
		} else {
			return this.kI;
		}
	}
	
	public void setkD(double kD)
	{
		this.kD = kD;
		if(this.anglePIDLoop != null)
		{
		this.anglePIDLoop.setPID(this.kP, this.kI, this.kD);
		}
	}
	
	public double getkD()
	{
		if(this.anglePIDLoop != null)
		{
		return this.anglePIDLoop.getD();
		} else {
			return this.kD;
		}
	}
}




/*if(Math.signum(leftValue) == 1.0 &&  Math.signum(rightValue) == 1.0 )
{ // If both sticks are pushed forward (Move forward)

  // Find the net direction we want to move
  if(Math.abs(leftValue) < Math.abs(rightValue))
  { // Net turn left
    move_forwardComponent = leftValue;
    move_leftComponent = rightValue - leftValue;

    turn_leftComponent = rightValue - leftValue;
  }
  else if(Math.abs(leftValue) > Math.abs(rightValue))
  { // Net turn right
    move_forwardComponent = rightValue;
    move_rightComponent = leftValue - rightValue;

    turn_rightComponent = rightValue - leftValue;
  }
  else if(Math.abs(leftValue) == Math.abs(rightValue))
  { // If the value is purely forward
    move_forwardComponent = rightValue;
  }

}
else if((Math.signum(leftValue) == -1.0 &&  Math.signum(rightValue) != -1.0 ) || 
      (Math.signum(leftValue) != 1.0 &&  Math.signum(rightValue) == 1.0 ))
{ // Left stick is negative, right is NOT negative OR Right is positive and left is NOT positive (Turn Left)

}
else if((Math.signum(leftValue) == 1.0 &&  Math.signum(rightValue) != 1.0 ) || 
    (Math.signum(leftValue) != -1.0 &&  Math.signum(rightValue) == -1.0 ))
{ // Left stick is positive, right is NOT positive OR Right is negative and left is NOT negative (Turn Left)

}
else if(Math.signum(leftValue) == -1.0 &&  Math.signum(rightValue) == -1.0 )
{ // If both sticks are pulled backward (Move backward)

  // Find the net direction we want to move
  if(Math.abs(leftValue) < Math.abs(rightValue))
  { // Net move back and left but turn to face the right direction
    move_backwardComponent = Math.abs(leftValue);
    move_leftComponent = Math.abs(rightValue) - Math.abs(leftValue);

    turn_rightComponent = Math.abs(rightValue) - Math.abs(leftValue);
  }
  else if(Math.abs(leftValue) > Math.abs(rightValue))
  { // Net move back and to the right but face to the left
    move_backwardComponent = rightValue;
    move_rightComponent = Math.abs(rightValue) - Math.abs(leftValue);

    turn_leftComponent = Math.abs(leftValue) - Math.abs(rightValue);
  }
  else if(Math.abs(leftValue) == Math.abs(rightValue))
  { // If the value is purely forward
    move_backwardComponent = rightValue;
  }
}*/
